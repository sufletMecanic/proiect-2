#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <map>
#include <utility>
#include <vector>
#include <sstream>
#include <iterator>

using namespace std;

const char lambda = '\0';
class AutomatFinitNedeterminist
{
private:
    static int s_identificatorUrmator;
    static int extrageUrmatorulIdentificator() { return s_identificatorUrmator++; }
    int m_stareInitiala;
    int m_stareFinala;
    multimap<pair<int, char>, int> m_functieTranzitie;

public:
    AutomatFinitNedeterminist() {}
    AutomatFinitNedeterminist(char litera)
    {
        m_stareInitiala = AutomatFinitNedeterminist::extrageUrmatorulIdentificator();
        m_stareFinala = AutomatFinitNedeterminist::extrageUrmatorulIdentificator();
        m_functieTranzitie.emplace(make_pair(m_stareInitiala, litera), m_stareFinala);
    }
    ~AutomatFinitNedeterminist() {}
    void adaugaLambdaTranzitie(const int stare1, const int stare2) { m_functieTranzitie.emplace(make_pair(stare1, lambda), stare2);}
    AutomatFinitNedeterminist stelare() const
    {
        AutomatFinitNedeterminist stelat;
        stelat.m_functieTranzitie = m_functieTranzitie;
        stelat.adaugaLambdaTranzitie(m_stareFinala, m_stareInitiala);
        stelat.m_stareInitiala = AutomatFinitNedeterminist::extrageUrmatorulIdentificator();
        stelat.adaugaLambdaTranzitie(stelat.m_stareInitiala, m_stareInitiala);
        stelat.m_stareFinala = AutomatFinitNedeterminist::extrageUrmatorulIdentificator();
        stelat.adaugaLambdaTranzitie(m_stareFinala, stelat.m_stareFinala);
        stelat.adaugaLambdaTranzitie(stelat.m_stareInitiala, stelat.m_stareFinala);
        return stelat;
    }
    friend ostream& operator<< (ostream &out, const AutomatFinitNedeterminist &a1);
    friend AutomatFinitNedeterminist operator+ (const AutomatFinitNedeterminist &a1, const AutomatFinitNedeterminist &a2);
    friend AutomatFinitNedeterminist operator* (const AutomatFinitNedeterminist &a1, const AutomatFinitNedeterminist &a2);
};
int AutomatFinitNedeterminist::s_identificatorUrmator{0};

ostream& operator<< (ostream &out, const AutomatFinitNedeterminist &a1)
{
    out << "Stare initiala: " << a1.m_stareInitiala << '\n'
        << "Stare finala: " << a1.m_stareFinala << '\n'
        << "Functie de tranzitie: " << '\n';
    for (auto it = a1.m_functieTranzitie.cbegin(); it != a1.m_functieTranzitie.cend(); it = a1.m_functieTranzitie.upper_bound(it->first))
    {
        out << '(' << it->first.first << ", ";
        if(it->first.second == lambda)
            out << "lambda";
        else
            out << it->first.second;
        out << ')' << " =>";
        auto ret = a1.m_functieTranzitie.equal_range(it->first);
        for (auto jt=ret.first; jt!=ret.second; ++jt)
            out << ' ' << jt->second;
        out << '\n';
    }
    return out;
}

AutomatFinitNedeterminist operator+ (const AutomatFinitNedeterminist &a1, const AutomatFinitNedeterminist &a2)
{
    AutomatFinitNedeterminist reuniune;
    reuniune.m_functieTranzitie = a1.m_functieTranzitie;
    reuniune.m_functieTranzitie.insert(a2.m_functieTranzitie.cbegin(), a2.m_functieTranzitie.cend());
    reuniune.m_stareInitiala = AutomatFinitNedeterminist::extrageUrmatorulIdentificator();
    reuniune.adaugaLambdaTranzitie(reuniune.m_stareInitiala, a1.m_stareInitiala);
    reuniune.adaugaLambdaTranzitie(reuniune.m_stareInitiala, a2.m_stareInitiala);
    reuniune.m_stareFinala = AutomatFinitNedeterminist::extrageUrmatorulIdentificator();
    reuniune.adaugaLambdaTranzitie(a1.m_stareFinala, reuniune.m_stareFinala);
    reuniune.adaugaLambdaTranzitie(a2.m_stareFinala, reuniune.m_stareFinala);
    return reuniune;
}

AutomatFinitNedeterminist operator* (const AutomatFinitNedeterminist &a1, const AutomatFinitNedeterminist &a2)
{
    AutomatFinitNedeterminist concatenare;
    concatenare.m_functieTranzitie = a2.m_functieTranzitie;
    for (auto it = concatenare.m_functieTranzitie.begin(); it != concatenare.m_functieTranzitie.end(); ++it)
    {
        if(it->first.first == a2.m_stareInitiala)
        {
            concatenare.m_functieTranzitie.emplace(make_pair(a1.m_stareFinala, it->first.second), it->second);
            it = concatenare.m_functieTranzitie.erase(it);
        }
    }
    concatenare.m_functieTranzitie.insert(a1.m_functieTranzitie.cbegin(), a1.m_functieTranzitie.cend());
    concatenare.m_stareInitiala = a1.m_stareInitiala;
    concatenare.m_stareFinala = a2.m_stareFinala;
    return concatenare;
}

class ExpresieRegulata
{
private:
    string m_expresie;
    char peek() { return m_expresie.front(); }
    char get()
    {
        char c = peek();
        m_expresie.erase(0, 1);
        return c;
    }
    AutomatFinitNedeterminist factor()
    {
        if(isalpha(peek()))
        {
            AutomatFinitNedeterminist rezultat = AutomatFinitNedeterminist(get());
            if(peek() == '*')
            {
                get();
                return rezultat.stelare();
            }
            return rezultat;
        }
        if(peek() == '(')
        {
            get();
            AutomatFinitNedeterminist rezultat = expresie();
            get();
            if(peek() == '*')
            {
                get();
                return rezultat.stelare();
            }
            return rezultat;
        }
        return AutomatFinitNedeterminist();
    }
    AutomatFinitNedeterminist termen()
    {
        AutomatFinitNedeterminist rezultat = factor();
        while(isalpha(peek()) || peek() == '(')
            rezultat = rezultat * factor();
        return rezultat;
    }
    AutomatFinitNedeterminist expresie()
    {
        AutomatFinitNedeterminist rezultat = termen();
        while(peek() == '|')
        {
            get();
            rezultat = rezultat + termen();
        }
        return rezultat;
    }
public:
    ExpresieRegulata(string expresie = ""): m_expresie{expresie} {}
    operator AutomatFinitNedeterminist() const
    {
        return ExpresieRegulata{m_expresie}.expresie();
    }
};

int main()
{
    ExpresieRegulata expresieRegulata("a*(a|b)*abb");
    cout << static_cast<AutomatFinitNedeterminist>(expresieRegulata);
    return 0;
}
